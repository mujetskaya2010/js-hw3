///1.Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
///
/// Цикли у програмуванні використовуються у випадку коли необхідно використати дію (код) необхідну кількість разів.
/// Для різних завдань програмування існують різні оператори циклу, за допомогою яких вони вирішуються набагато простіше.

///2.Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

/// У JavaScript є такі цикли:
///for
///for ... in
///for ... of
///while
///do...while

/// Цикл for, виконує код доки умова дорівнює true.
/// Цикл for... in, призначений для проходження по властивостях об'єкту.
/// Цикл for ... of, призначений для проходження по спископодібних об'єктах.

/// let arr = [3, 5, 7];

///for (let i of arr) {
///  console.log(i);
///}

/// Цикл while, виконується доки умова дорівнює true.
///let i=1;
///while(i<10){
///console.log(i);
///i++;
///}

///Цикл do...while, цей цикл спочатку виконає тіло, а потім перевірить
///умову, і доки його значення дорівнює true, він буде виконуватися знову і знову.
///let i = 0;
///do {
/// alert( i );
/// i++;
//} while (i < 3);

///3. Що таке явне та неявне приведення (перетворення) типів даних у JS?

///Коли розробник висловлює намір сконвертувати значення одного типу в значення іншого типу, записуючи це відповідним чином коді,
///наприклад як Number(value), це називається явним приведенням типів (явним перетворенням типів).
///Якщо у виразах використовують значення різних типів то ці значення можуть бути конвертовані автоматично.
/// Це називають неявним наведенням типів.

let n = +prompt("Your number?");
let i = 0;

while (Number.isInteger(n) == false) {
  n = +prompt("Your number?");
}
while (i < n && Number.isInteger(n) == true)
  if (n >= 5 && n % 5 == 0) {
    i = i + 5;
    console.log(i);
  } else if (n >= 5 && n % 5 != 0) {
    i = i + 5;
    console.log(i - 5);
  } else if (n < 5) {
    console.log("Sorry, no numbers");
    break;
  } else {
    console.log("Sorry, no numbers");
    break;
  }
